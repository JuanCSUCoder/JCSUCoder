### Hi there 👋
---
## My name is Juan Camilo Sánchez Urrego
I'm a 17 years old self-taught programmer from Colombia, that is fascinated with everything related to computer science. I have learn a lot from the internet since 12 years old, and developed some interesting projects that I'll be updating on here.

[Platzi Profile](https://platzi.com/@juancsucoder/)

## Platzi Certificates
- [Fundamentals of Software Engineering](https://platzi.com/@juancamilosanchezurrego/curso/1098-ingenieria/diploma/detalle/)
- [Internet Networks](https://platzi.com/@juancamilosanchezurrego/curso/1277-redes/diploma/detalle/)
- [Fundamentals of Docker](https://platzi.com/@juancamilosanchezurrego/curso/1432-docker/diploma/detalle/)
- [React Router](https://platzi.com/@juancamilosanchezurrego/curso/1342-react-router/diploma/detalle/)
- [Kubernetes "K8s"](https://platzi.com/@juancamilosanchezurrego/curso/1565-k8s/diploma/detalle/)
---
- 🔭 I’m currently working on ... **Learning**
- 🌱 I’m currently learning ...
  - RUST Language
  - GlusterFS
  - Ceph Storage
  - Go Language
- 👯 I’m looking to collaborate on ...
[Google Meet Grid View (Fix) Extension](https://github.com/icysapphire/google-meet-grid-view)
- 💬 Ask me about ... **Everything**
- 📫 How to reach me: ...
  - Instagram: [@juancsucoder](https://www.instagram.com/juancsucoder/)
  - Twitter: [@juancsucoder](https://twitter.com/juancsucoder)
  - LinkedIn: [Juan Camilo Sánchez Urrego](https://www.linkedin.com/in/juancsucoder/)
  - Platzi: [@juancsucoder](https://platzi.com/@juancsucoder/)
